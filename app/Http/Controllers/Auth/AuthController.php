<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    function login(Request $request)
    {
        $validate = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (Auth::attempt($validate)) {
            $token = Auth::guard('api')->attempt($validate);

            return response()->json([
                'status_code' => 200,
                'message' => 'Login success',
                'data' => Auth::user(),
                'token' => $token
            ], 200);
        }

        return response()->json([
            'status_code' => 404,
            'message' => 'Username or password is incorrect',
        ], 404);
    }

    function logout(Request $request)
    {
        Auth::guard('api')->logout();

        return response()->json([
            'status_code' => 200,
            'message' => 'Logout success',
        ], 200);
    }

    function testAuth()
    {
        return response()->json([
            'status_code' => 200,
            'message' => 'You are authorized',
            'data' => Auth::user()
        ], 200);
    }
}
