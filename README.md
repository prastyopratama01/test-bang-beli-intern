## Requirements

-   "php": "^8.0.2"
-   "laravel/framework": "^9.19"

## Installation

create new db and in .env rename value DB_DATABASE to new database before creating

```
composer install
```

```
php artisan jwt:secret
```

```
php artisan migrate:fresh --seed
```

and for running server

```
php artisan serve
```

## docs

link postman docs : https://www.postman.com/restless-spaceship-236926/workspace/test-code/collection/23487853-efe6916e-ac9f-43c9-8953-b62d590e6931?action=share&creator=23487853

Account login apps

-   User 1
-   password
-   User 2
-   password
