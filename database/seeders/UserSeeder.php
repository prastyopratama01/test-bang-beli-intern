<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'user 1',
            'password' => bcrypt('password')
        ]);

        User::create([
            'username' => 'user 2',
            'password' => bcrypt('password')
        ]);
    }
}
